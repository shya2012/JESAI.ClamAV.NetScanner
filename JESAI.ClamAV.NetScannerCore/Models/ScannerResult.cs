﻿using JESAI.ClamAV.NetScannerCore.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace JESAI.ClamAV.NetScannerCore.Models
{
    public class ScannerResult 
    {  
     public ScannerResult(ScanResponseStatus status,
            IReadOnlyCollection<InfectedFile> infectedFiles,
            string rawResult)
    {
        this.Status = status;
        this.InfectedFiles = infectedFiles;
        this.RawResult = rawResult;
    }

    public ScanResponseStatus Status { get; }

    public IReadOnlyCollection<InfectedFile> InfectedFiles { get; }

    public string RawResult { get; }

    public override string ToString()
    {
        return this.RawResult;
    }
}
}