﻿using JESAI.ClamAV.NetScannerCore.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace JESAI.ClamAV.NetScannerCore.Command
{
    public interface ICommandBuilder
    {
        ICommandBuilder SetCommand(ClamAVCommand command);
        ICommandBuilder CreateCommand();
        ICommandBuilder CommantToBytes();
        byte[] Build();
    }
}
