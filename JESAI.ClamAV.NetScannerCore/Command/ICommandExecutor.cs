﻿using JESAI.ClamAV.NetScannerCore.Enums;
using JESAI.ClamAV.NetScannerCore.TcpClients;
using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace JESAI.ClamAV.NetScannerCore.Command
{
    public interface ICommandExecutor
    {
        IScannerTCPClient SocketClient { get; }

        string Execute(ClamAVCommand command, Func<NetworkStream, string> additionalCommand = null);
        Task<string> ExecuteAsync(ClamAVCommand command, Func<NetworkStream, Task> additionalCommand = null);
        Task<string> ExecuteAsync(ClamAVCommand command, CancellationToken cancellationToken, Func<NetworkStream, CancellationToken, Task> additionalCommand = null);
        string Execute(string command, Func<NetworkStream, string> additionalCommand = null);
        Task<string> ExecuteAsync(string command, Func<NetworkStream, Task> additionalCommand = null);
        Task<string> ExecuteAsync(string command, CancellationToken cancellationToken, Func<NetworkStream, CancellationToken, Task> additionalCommand = null);
    }
}
