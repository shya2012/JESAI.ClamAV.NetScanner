﻿using JESAI.ClamAV.NetScannerCore.Enums;
using JESAI.ClamAV.NetScannerCore.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace JESAI.ClamAV.NetScannerCore.Command
{
    public class CommandBuilder : ICommandBuilder
    {
        private ClamAVCommand _command;
        private string commandString;
        private byte[] bytes;
        public byte[] Build()
        {
            if (bytes.Length <= 0)
            {
                throw new ArgumentNullException("字节数组为空，请先CommantToBytes创建命令字节数组!");
            }
            return bytes;
        }

        public ICommandBuilder CommantToBytes()
        {
            if (commandString.IsNullOrEmpty())
            {
                throw new ArgumentNullException("命令字符串不能为空，请先CreateCommand创建命令字符串!");
            }
            bytes=Encoding.UTF8.GetBytes($"z{commandString}\0");
            return this;
        }

        public ICommandBuilder CreateCommand()
        {
            if (!Enum.TryParse(typeof(ClamAVCommand),_command.ToString(),out object? result))
            {
                throw new ArgumentNullException("命令不能为空，请先SetCommand设置命令!");
            }
            commandString = $"{_command}".ToUpperInvariant();
            return this;
        }

        public ICommandBuilder SetCommand(ClamAVCommand command)
        {
            this._command = command;
            return this;
        }
    }
}
