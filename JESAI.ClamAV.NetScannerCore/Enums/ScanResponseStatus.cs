﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JESAI.ClamAV.NetScannerCore.Enums
{
    public enum ScanResponseStatus
    {
        Unknown,

        Clean,

        VirusDetected,

        Error
    }
}
