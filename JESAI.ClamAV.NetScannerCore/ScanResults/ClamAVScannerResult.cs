﻿using JESAI.ClamAV.NetScannerCore.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;

namespace JESAI.ClamAV.NetScannerCore.ScanResults
{
    [Serializable]
    public class ClamAVScannerResult
    {
        public ClamAVScannerResult(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                throw new ClamAVScannerResultEmptyOrNullException();
            }

            this.Value = value;
        }
        public string Value { get; }

    }
}
