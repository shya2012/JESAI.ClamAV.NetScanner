﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JESAI.ClamAV.NetScannerCore.Exceptions
{
    public class StreamSizeOverFlowException:Exception
    {
        public StreamSizeOverFlowException(long maxStreamSize)
            : base($"字节流溢出!已超过{maxStreamSize}字节的最大流大小。")
        {
        }
    }
}
