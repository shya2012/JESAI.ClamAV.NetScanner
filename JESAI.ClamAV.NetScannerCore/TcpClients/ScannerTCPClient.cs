﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;

namespace JESAI.ClamAV.NetScannerCore.TcpClients
{
    public class ScannerTCPClient : TcpClient, IScannerTCPClient
    {
        bool IScannerTCPClient.Active { get{ return this.Active; } set { this.Active = value; } }
    }
}
