﻿using JESAI.ClamAV.NetScannerCore.Command;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace JESAI.ClamAV.NetScannerCore.TcpClients.Builder
{
    public interface IScannerTcpClientBuilder
    {       
        IScannerTCPClient Build();
        Task<IScannerTCPClient> BuildAsync();
        void Close();
    }
}
