﻿using JESAI.ClamAV.NetScannerCore.Models;
using JESAI.DynamicWebApi;
using JESAI.DynamicWebApi.Attributes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace JESAI.ClamAV.NetScannerCore.Rest.DynamicWebApi
{
    [DynamicWebApi]
    public class ClamAVVerionsApplicationService : IDynamicWebApiService
    {
        private readonly IClamAVScannerClient ClamAVScannerClient;
        public ClamAVVerionsApplicationService(IClamAVScannerClient clamAVScannerClient)
        {
            ClamAVScannerClient = clamAVScannerClient;
        }
        /// <summary>
        /// 获取ClamAV服务器版本
        /// </summary>
        public Task<string> GetVersionAsync() 
        {
            return ClamAVScannerClient.GetVersionAsync();
        }    
    }
}
