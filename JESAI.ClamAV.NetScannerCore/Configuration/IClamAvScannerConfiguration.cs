﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JESAI.ClamAV.NetScannerCore.Configuration
{
    public interface IClamAvScannerConfiguration
    {
        /// <summary>
        /// 发送到ClamAV服务器时，流将被分解到的最大大小（字节）。在SendAndScanFile方法中使用。默认大小为128kb。
        /// </summary>
        int MaxChunkSize { get; set; }
        /// <summary>
        /// 在ClamAV服务器终止连接之前，可以传输到该服务器的最大大小（字节）。在SendAndScanFile方法中使用。默认大小为25mb。
        /// </summary>
        long MaxStreamSize { get; set; }
        /// <summary>
        /// ClamAV服务器的地址
        /// </summary>
        string Host { get; set; }

        /// <summary>
        /// ClamAV服务器正在监听的端口
        /// </summary>
        int Port { get; set; }
    }
}
