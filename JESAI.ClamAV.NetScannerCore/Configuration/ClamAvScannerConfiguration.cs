﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JESAI.ClamAV.NetScannerCore.Configuration
{
    public class ClamAvScannerConfiguration : IClamAvScannerConfiguration
    {
        public int MaxChunkSize { get; set; }
        public long MaxStreamSize { get; set; }
        public string Host { get; set; }
        public int Port { get; set; }
        public ClamAvScannerConfiguration()
        {
            MaxChunkSize = AppConst.MaxChunkSize;
            MaxStreamSize = AppConst.MaxStreamSize;
            Host = AppConst.Host;
            Port = AppConst.Port;
        }
    }
}
