﻿using JESAI.ClamAV.NetScannerCore;
using JESAI.ClamAV.NetScannerCore.Enums;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace JESAI.ClamAV.NetScanner.Test
{
    public class ScanClientTests
    {
        [Fact]
        public async Task ScanBytes_InfectedData_ReturnsExpectedResult()
        {
            var ctx = IntegrationTestContext.CreateDefaultUnitTestContext();
            const string sampleVirusSignature = @"X5O!P%@AP[4\PZX54(P^)7CC)7}$EICAR-STANDARD-ANTIVIRUS-TEST-FILE!$H+H*";
            IClamAVScannerClient ClamAVScannerClient=ctx.Resolve<IClamAVScannerClient>();
            var bytes = Encoding.UTF8.GetBytes(sampleVirusSignature);
           var result=await ClamAVScannerClient.SendAndScanFileAsync(bytes);
            Assert.Equal(ScanResponseStatus.VirusDetected, result.Status);
            Assert.NotEmpty(result.InfectedFiles);
        }
        [Fact]
        public async Task ScanFile_InfectedData_ReturnsOKResult()
        {
            var ctx = IntegrationTestContext.CreateDefaultUnitTestContext();
            string filePath = @"C:\Users\deng\Desktop\test.rar";
            IClamAVScannerClient ClamAVScannerClient = ctx.Resolve<IClamAVScannerClient>();        
            var result = await ClamAVScannerClient.ScanFileOnServerAsync(filePath);
            Assert.NotEqual(ScanResponseStatus.Clean, result.Status);
            //Assert.NotEmpty(result.InfectedFiles);
        }
        [Fact]
        public async Task ScanFile_InfectedData_ReturnsVirusDetectedResult()
        {
            var ctx = IntegrationTestContext.CreateDefaultUnitTestContext();
            string filePath = @"C:\Users\deng\Desktop\test.rar";
            IClamAVScannerClient ClamAVScannerClient = ctx.Resolve<IClamAVScannerClient>();
            var result = await ClamAVScannerClient.ScanFileOnServerAsync(filePath);
            Assert.Equal(ScanResponseStatus.VirusDetected, result.Status);
            //Assert.NotEmpty(result.InfectedFiles);
        }
        [Fact]
        public async Task ScanFileByte_InfectedData_ReturnsVirusDetectedResult()
        {
            var ctx = IntegrationTestContext.CreateDefaultUnitTestContext();
            string filePath = @"C:\Users\deng\Desktop\b.zip";

            IClamAVScannerClient ClamAVScannerClient = ctx.Resolve<IClamAVScannerClient>();
            var result =await ClamAVScannerClient.SendAndScanFileAsync(File.ReadAllBytes(filePath));
            
            Assert.Equal(ScanResponseStatus.VirusDetected, result.Status);
            //Assert.NotEmpty(result.InfectedFiles);
        }
        [Fact]
        public async Task ScanFileStream_InfectedData_ReturnsVirusDetectedResult()
        {
            var ctx = IntegrationTestContext.CreateDefaultUnitTestContext();
            string filePath = @"C:\Users\deng\Desktop\b.zip";

            IClamAVScannerClient ClamAVScannerClient = ctx.Resolve<IClamAVScannerClient>();
            var result = await ClamAVScannerClient.SendAndScanFileAsync(FileToStream(filePath));

            Assert.Equal(ScanResponseStatus.VirusDetected, result.Status);
            //Assert.NotEmpty(result.InfectedFiles);
        }
        protected byte[] FileToStream(string fileName)

        {

            // 打开文件

            FileStream fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read);

            // 读取文件的 byte[]

            byte[] bytes = new byte[fileStream.Length];

            fileStream.Read(bytes, 0, bytes.Length);

            fileStream.Close();

            // 把 byte[] 转换成 Stream

            //Stream stream = new MemoryStream(bytes);

            return bytes;

        }
        protected byte[] AuthGetFileData(string fileUrl)
        {
            using (FileStream fs = new FileStream(fileUrl, FileMode.OpenOrCreate, FileAccess.ReadWrite))
            {
                byte[] buffur = new byte[fs.Length];
                using (BinaryWriter bw = new BinaryWriter(fs))
                {                  
                    bw.Write(buffur);
                    bw.Close();
                }
                return buffur;
            }
        }

       
    }
}
