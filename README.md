# JESAI.ClamAV.NetScanner

#### 介绍
本项目是一个互联网云查杀方案，主要是利用TCP通信和ClamAV服务器交互，从而进行一些文件或者需要查杀的东西在云端杀毒，当然了，ClamAV也可以本地杀毒，使用本项目的前提，你要学会搭建ClamAV服务器。详情请看：[https://www.clamav.net/](https://www.clamav.net/)。本类库目前是netcore3.1平台。

#### clamav
clamav是思科开源的一个杀毒引擎。ClamAV的®是一个开源（GPL）的各种情况，包括电子邮件扫描，Web扫描和终点的安全使用防病毒引擎。它提供了许多实用程序，包括灵活且可扩展的多线程守护程序，命令行扫描程序和用于自动数据库更新的高级工具。官网有完善的文档和工具，具体请自行研究。
![输入图片说明](https://images.gitee.com/uploads/images/2020/0307/235014_7a733b72_370556.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/0307/235210_09b94e72_370556.png "屏幕截图.png")


#### 安装教程

1.  首先你要去官网下载ClamAV程序，并在服务器或者本地部署ClamAV服务，然后启动服务。如下是启动效果。
![输入图片说明](https://images.gitee.com/uploads/images/2020/0307/235635_25370f06_370556.png "屏幕截图.png")
2.  nuget 包管理器搜索 JESAI.ClamAV.NetScannerCore 
或者使用命令：Install-Package JESAI.ClamAV.NetScannerCore

#### 使用说明

1.  Starup ConfigureServices里面添加
如果服务器配置，则添加

```
services.AddClamAVNetScanner();
```
如果需要自定义配置信息，则增加如下代码

```
 //IClamAvScannerConfiguration不配置的话，会默认
            //Host = "127.0.0.1",
            //Port = 3310,
            //MaxChunkSize = 2014,
            //MaxStreamSize = 26214400
            IClamAvScannerConfiguration configuration = new ClamAvScannerConfiguration()
            {
                Host="127.0.0.1",
                Port=3310,
                MaxChunkSize=2014,
                MaxStreamSize= 26214400
            };
            services.AddClamAVNetScanner(configuration);
            //也可以这样用
            //services.AddClamAVNetScanner((s) => { s.AddSingleton<IClamAvScannerConfiguration>(configuration); });
```

然后在services.AddControllersWithViews();后面加上services.AddClamAVNetScannerDynamicWebApi();可以不加，不加的话WebApi不会生效。

```
services.AddControllersWithViews();
services.AddClamAVNetScannerDynamicWebApi();
```
![输入图片说明](https://images.gitee.com/uploads/images/2020/0308/002042_388675d8_370556.png "屏幕截图.png")

2. Starup Configure里面配置，这个主要是swagger用，如果不使用WebApi，也可以不配置。

```
app.UseClamAVNetScanner();
```

3.  注入Client服务。

```
 private readonly IClamAVScannerClient ClamAVScannerClient;
 public HomeController(ILogger<HomeController> logger, IClamAVScannerClient clamAVScannerClient)
 {
       _logger = logger;
      ClamAVScannerClient = clamAVScannerClient;
 }
```


3.1    获取ClamAV服务器版本
GetVersionAsync;

3.2    在ClamAV服务器上执行PING命令。
PingAsync;

3.3    扫描ClamAV服务器上的文件/目录。
ScanFileOnServerAsync;

3.4    使用服务器上的多个线程扫描ClamAV服务器上的文件/目录。
ScanFileOnServerMultithreadedAsync;

3.5    将数据作为流发送到ClamAV服务器(Byte)。
SendAndScanFileAsync;

3.6    将数据作为流发送到ClamAV服务器(Byte)。
SendAndScanBytesAsync;

3.6    将数据作为流发送到ClamAV服务器(Stream)。
SendAndScanFileAsync;

3.7    从路径读取文件，然后将其作为流发送到ClamAV服务器。
SendAndScanFileAsync;

```
using JESAI.ClamAV.NetScannerCore.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace JESAI.ClamAV.NetScannerCore
{
    public interface IClamAVScannerClient
    {      
        /// <summary>
        /// 获取ClamAV服务器版本
        /// </summary>
        Task<string> GetVersionAsync();

        /// <summary>
        /// 获取ClamAV服务器版本
        /// </summary>
        /// <param name="cancellationToken">用于请求的取消令牌</param>
        Task<string> GetVersionAsync(CancellationToken cancellationToken);

        /// <summary>
        /// 在ClamAV服务器上执行PING命令。
        /// </summary>
        /// <returns>如果服务器以PONG响应，则返回true。否则返回false。</returns>
        Task<bool> PingAsync();

        /// <summary>
        /// 在ClamAV服务器上执行PING命令。
        /// </summary>
        /// <param name="cancellationToken">用于请求的取消令牌</param>
        /// <returns>如果服务器以PONG响应，则返回true。否则返回false。</returns>
        Task<bool> PingAsync(CancellationToken cancellationToken);

        /// <summary>
        /// 扫描ClamAV服务器上的文件/目录。
        /// </summary>
        /// <param name="filePath">ClamAV服务器上文件/目录的路径。</param>
        Task<ScannerResult> ScanFileOnServerAsync(string filePath);

        /// <summary>
        /// 扫描ClamAV服务器上的文件/目录。
        /// </summary>
        /// <param name="filePath">ClamAV服务器上文件/目录的路径。</param>
        /// <param name="cancellationToken">用于请求的取消令牌</param>
        Task<ScannerResult> ScanFileOnServerAsync(string filePath, CancellationToken cancellationToken);

        /// <summary>
        /// 使用服务器上的多个线程扫描ClamAV服务器上的文件/目录。
        /// </summary>
        /// <param name="filePath">ClamAV服务器上文件/目录的路径。</param>
        Task<ScannerResult> ScanFileOnServerMultithreadedAsync(string filePath);

        /// <summary>
        /// 使用服务器上的多个线程扫描ClamAV服务器上的文件/目录。
        /// </summary>
        /// <param name="filePath">ClamAV服务器上文件/目录的路径。</param>
        /// <param name="cancellationToken">用于请求的取消令牌</param>
        Task<ScannerResult> ScanFileOnServerMultithreadedAsync(string filePath, CancellationToken cancellationToken);

        /// <summary>
        /// 将数据作为流发送到ClamAV服务器。
        /// </summary>
        /// <param name="fileData">包含文件数据的字节数组。</param>
        /// <returns></returns>
        Task<ScannerResult> SendAndScanFileAsync(byte[] fileData);
        /// <summary>
        /// 将数据作为流发送到ClamAV服务器。
        /// </summary>
        /// <param name="fileData">包含文件数据的字节数组。</param>
        /// <returns></returns>
        Task<ScannerResult> SendAndScanBytesAsync(byte[] bytes);

        /// <summary>
        /// 将数据作为流发送到ClamAV服务器。
        /// </summary>
        /// <param name="fileData">包含文件数据的字节数组。</param>
        /// <param name="cancellationToken">用于请求的取消令牌</param>
        /// <returns></returns>
        Task<ScannerResult> SendAndScanFileAsync(byte[] fileData, CancellationToken cancellationToken);

        /// <summary>
        /// 将数据作为流发送到ClamAV服务器。
        /// </summary>
        /// <param name="sourceStream">包含要扫描的数据的流。</param>
        /// <returns></returns>
        Task<ScannerResult> SendAndScanFileAsync(Stream sourceStream);

        /// <summary>
        /// 将数据作为流发送到ClamAV服务器。
        /// </summary>
        /// <param name="sourceStream">包含要扫描的数据的流。</param>
        /// <param name="cancellationToken">用于请求的取消令牌</param>
        /// <returns></returns>
        Task<ScannerResult> SendAndScanFileAsync(Stream sourceStream, CancellationToken cancellationToken);

        /// <summary>
        /// 从路径读取文件，然后将其作为流发送到ClamAV服务器。
        /// </summary>
        /// <param name="filePath">文件/目录的路径。</param>
        Task<ScannerResult> SendAndScanFileAsync(string filePath);

        /// <summary>
        /// 从路径读取文件，然后将其作为流发送到ClamAV服务器。
        /// </summary>
        /// <param name="filePath">文件/目录的路径。</param>
        /// <param name="cancellationToken">用于请求的取消令牌</param>
        Task<ScannerResult> SendAndScanFileAsync(string filePath, CancellationToken cancellationToken);
    }
}

```



#### 运行效果

```
[Fact]
       public async Task ScanBytes_InfectedData_ReturnsExpectedResult()
       {
           var ctx = IntegrationTestContext.CreateDefaultUnitTestContext();
           const string sampleVirusSignature = @"1111111111111";
           IClamAVScannerClient ClamAVScannerClient=ctx.Resolve<IClamAVScannerClient>();
           var bytes = Encoding.UTF8.GetBytes(sampleVirusSignature);
          var result=await ClamAVScannerClient.SendAndScanFileAsync(bytes);
           Assert.Equal(ScanResponseStatus.VirusDetected, result.Status);
           Assert.NotEmpty(result.InfectedFiles);
       }
```
![输入图片说明](https://images.gitee.com/uploads/images/2020/0308/000126_505dfd3e_370556.png "屏幕截图.png")

```
[Fact]
       public async Task ScanFileStream_InfectedData_ReturnsVirusDetectedResult()
       {
           var ctx = IntegrationTestContext.CreateDefaultUnitTestContext();
           string filePath = @"C:\Users\deng\Desktop\b.zip";
 
           IClamAVScannerClient ClamAVScannerClient = ctx.Resolve<IClamAVScannerClient>();
           var result = await ClamAVScannerClient.SendAndScanFileAsync(FileToStream(filePath));
 
           Assert.Equal(ScanResponseStatus.VirusDetected, result.Status);
           //Assert.NotEmpty(result.InfectedFiles);
       }
```
![输入图片说明](https://images.gitee.com/uploads/images/2020/0308/000211_ae3cc324_370556.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0308/000228_fd953d31_370556.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/0307/235924_e4d63672_370556.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/0308/000243_29e98adc_370556.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/0308/000259_8cc85fbd_370556.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/0308/000306_468235c6_370556.png "屏幕截图.png")